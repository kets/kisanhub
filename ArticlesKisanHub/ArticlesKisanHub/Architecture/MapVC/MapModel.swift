//
//  MapModel.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 02/10/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps

class MapModel: NSObject {

    var arrFeature = [CLLocationCoordinate2D]()
    var arrPolygon = [[CLLocationCoordinate2D]]()
    
    func getListOfLocationData(success: @escaping (MapModel) -> Void, failed:@escaping (String?) -> Void) {
        
        BaseClass.GETRequset(showLoader: false, url: Key.APIMAPUrl) { (response, error) in
            
            if(response == nil) {
                failed(error)
            }
            else {
                self.parsingResponseData(response: response, callBack: success)
            }
        }
    }
}

//MARK:- Parsing Response

extension MapModel {
    
    private func parsingResponseData(response: [Any]?, callBack: @escaping (MapModel) -> Void) {
        
        var dictData = JSON(response as Any).arrayValue[0].dictionaryValue
        self.parsePolygonData(arrPolygon: dictData[Key.fields]!.arrayValue)
        self.parsePinData(arrFeature: dictData[Key.farms]!.arrayValue)
        callBack(self)
    }
    
    private func parsePolygonData(arrPolygon: [JSON]) {
        
        for polygonInfo in arrPolygon {
            
            let arrPolygonList = (polygonInfo.dictionaryValue[Key.geometry]!.dictionaryValue[Key.coordinates]!.arrayValue[0].arrayValue)
            
            var objPolygon = [CLLocationCoordinate2D]()
            
            for polygonDetail in arrPolygonList {
                let polygonData = (polygonDetail.arrayValue)
                objPolygon.append(CLLocationCoordinate2DMake((polygonData[0].doubleValue), (polygonData[1].doubleValue)))
            }
            
            self.arrPolygon.append(objPolygon)
            
        }
    }
    
    func parsePinData(arrFeature: [JSON]) {
        
        for featureInfo in arrFeature {
            let lat =  (featureInfo.dictionaryValue[Key.geometry]?.dictionaryValue[Key.coordinates]?.arrayValue[0].doubleValue)!
            let lon =  (featureInfo.dictionaryValue[Key.geometry]?.dictionaryValue[Key.coordinates]?.arrayValue[1].doubleValue)!
            self.arrFeature.append(CLLocationCoordinate2DMake(lat, lon))
        }
    }
}
