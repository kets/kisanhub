//
//  MapVC.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 02/10/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit
import GoogleMaps

class MapVC: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    
    var arrRects = [GMSMutablePath]()
    let objMapDetails = MapModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getLocationDataFromServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- IBAction Methods
    
    @IBAction func btnBackCliecked(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API call
    
    private func getLocationDataFromServer() {
        objMapDetails.getListOfLocationData(success: { (model) in
            self.setUpAndRenderMap()
        }) { (error) in
            
        }
    }
    
    //MARK:- Custom Methods
    
    private func setUpAndRenderMap() {
        
        DispatchQueue.main.async {
            self.mapView.mapType = .hybrid
        }
                
        self.createRect()
        self.drawPolygon()
        self.dropPins()
    }
}

//MARK:- Render Map

extension MapVC {
    
    private func createRect() {
        
        for arrPolygon in objMapDetails.arrPolygon {
            
            let rect = GMSMutablePath()
            
            for arrChildPolygon in arrPolygon {
                rect.add(arrChildPolygon)
            }
            
            arrRects.append(rect)
        }
    }
    
    private func drawPolygon() {
        
        for arrListOfPoint in arrRects {
            
            let rectList = arrListOfPoint
            let color = getRandomColor()
        
            if(rectList.count() > 2) {
                
                for index in 0..<rectList.count() {
                    self.createMarker(color: color, position: rectList.coordinate(at: index))
                }
                
                let polygon = GMSPolygon(path: arrListOfPoint)
                polygon.fillColor = color.withAlphaComponent(0.4)
                polygon.strokeColor = color
                polygon.strokeWidth = 2
                polygon.map = mapView
            }
        }
        
    }
    
    private func dropPins() {
        let color = getRandomColor()
        
        for markerDetail in objMapDetails.arrFeature {
            self.createMarker(color: color, position: markerDetail)
        }
    }

    private func getRandomColor() -> UIColor {
        let red:CGFloat = CGFloat(drand48())
        let green:CGFloat = CGFloat(drand48())
        let blue:CGFloat = CGFloat(drand48())
        return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
    }
    
    private func createMarker(color: UIColor, position: CLLocationCoordinate2D) {
        let marker = GMSMarker()
        marker.icon = GMSMarker.markerImage(with: color)
        marker.position = position
        marker.isDraggable = true
        //marker.userData = userData
        marker.map = mapView
    }
}
