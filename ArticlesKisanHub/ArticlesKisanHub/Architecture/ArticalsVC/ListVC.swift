//
//  ListVC.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 30/09/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit

class ListVC: UIViewController {

    var arrList = [CardModel]()
    var objCardeModel = CardModel()
    
    @IBOutlet weak var cvList: UICollectionView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getListOfCards()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func btnBackCliecked(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- API CALL
    
    private func getListOfCards() {
        
        objCardeModel.getAllCardList(showLoader: false, parameter: nil, success: { (arrCardList) in
            
            self.arrList = arrCardList
            
            DispatchQueue.main.async {
                self.cvList.reloadData()
            }
            
        }) { (strError) in
            
        }
    }
}

extension ListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrList.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CardCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath) as! CardCell
        cell.cardeDetail = arrList[indexPath.row]
        return cell
    }
 
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        btnNext.isHidden = false
        btnPrevious.isHidden = false
        
        if(indexPath.row == 0) {
            btnPrevious.isHidden = true
        }
        else if(indexPath.row+1 == arrList.count) {
            btnNext.isHidden = true
        }
    }
}
