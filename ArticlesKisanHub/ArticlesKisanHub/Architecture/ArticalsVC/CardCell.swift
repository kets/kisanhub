//
//  CardCell.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 30/09/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit
import SDWebImage

class CardCell: UICollectionViewCell {

    @IBOutlet weak var ivCardImage: UIImageView!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewAutherDetail: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivAutherImage: UIImageView!
    @IBOutlet weak var lblAutherName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewDetail.layer.cornerRadius = 5
        self.viewAutherDetail.layer.cornerRadius = 3
        self.ivAutherImage.layer.cornerRadius = self.ivAutherImage.frame.size.height/2
    }

    var cardeDetail: CardModel? = nil {
        didSet {
            
            self.lblAutherName.text = cardeDetail?.strAutherName
            self.lblTitle.text = cardeDetail?.strTitle
            self.lblDesc.text = cardeDetail?.strDesc
            
            self.ivAutherImage.sd_setImage(with: cardeDetail?.urlAutherImage, placeholderImage: #imageLiteral(resourceName: "UserPlaceholder")) { (image, error, nil, url) in
                
            }
            
            self.ivCardImage.sd_setImage(with: cardeDetail?.urlCardImage, placeholderImage: #imageLiteral(resourceName: "BookPlaceholder")) { (image, error, nil, url) in
                
            }
        }
    }
}
