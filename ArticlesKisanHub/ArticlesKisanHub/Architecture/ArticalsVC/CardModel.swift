//
//  CardModel.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 30/09/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit
import SwiftyJSON

class CardModel: NSObject {
    
    var strTitle: String!
    var urlCardImage: URL!
    var urlAutherImage: URL!
    var strDesc: String!
    var strAutherName: String!
    
    func getAllCardList(showLoader:(Bool), parameter:[String : String]?, success:@escaping ([CardModel]) -> Void, failed:@escaping (String?) -> Void) {
        
        NetworkHandler.GETRequset(showLoader: showLoader, url: Key.APIBooksUrl) { (dictResponse, errorMsg) in
            
            if(errorMsg == nil) {
                
                let data = JSON(dictResponse!).dictionary
                let arrList = data![Key.data]?.arrayValue
                self.parsingcardList(arrcardList: arrList!, success: success)
            }
            else {
                failed(errorMsg)
            }
        }
    }
    
    private func parsingcardList(arrcardList:[JSON], success:@escaping ([CardModel]) -> Void) {
        
        var arrList = [CardModel]()
        
        for dict in arrcardList {
            
            let cardDetail = CardModel()
            cardDetail.strTitle = dict[Key.title].stringValue
            
            let arrImages = dict[Key.featuredImage].arrayValue
            if(arrImages.count > 0) {
                cardDetail.urlCardImage = URL(string: arrImages[0].dictionaryValue[Key.imageFile]?.stringValue ?? "")
            }
            
            let arrAuther = dict[Key.authors].arrayValue
            if(arrAuther.count > 0) {
                cardDetail.strAutherName = arrAuther[0].dictionaryValue[Key.fullName]?.stringValue ?? ""
                cardDetail.urlAutherImage = URL(string: arrAuther[0].dictionaryValue[Key.picture]?.stringValue ?? "")
            }
            
            cardDetail.strDesc = dict[Key.description].stringValue
            
            arrList.append(cardDetail)
        }
        
        success(arrList)
    }
    
}
