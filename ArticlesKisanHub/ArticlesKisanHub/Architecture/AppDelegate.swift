//
//  AppDelegate.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 30/09/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyDIX2RDVqIjZbTT97gHHTnASEQ0rTu8ezY")
        return true
    }
}

