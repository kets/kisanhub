//
//  OptionsVC.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 02/10/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit

class OptionsVC: UIViewController {

    @IBOutlet weak var btnArtical: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnMap.layer.cornerRadius = 5
        btnArtical.layer.cornerRadius = 5
    }
}
