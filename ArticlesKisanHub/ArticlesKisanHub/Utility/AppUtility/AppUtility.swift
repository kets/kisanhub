//
//  AppUtility.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 02/10/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit

struct Key {
    static let data             = "data"
    static let title            = "title"
    static let featuredImage    = "featured_image"
    static let imageFile        = "image_file"
    static let authors          = "authors"
    static let fullName         = "full_name"
    static let picture          = "picture"
    static let description      = "description"
    static let geometry         = "geometry"
    static let coordinates      = "coordinates"
    static let fields           = "fields"
    static let farms            = "farms"
    static let APIMAPUrl        = "https://s3.eu-west-2.amazonaws.com/interview-question-data/farm/farms.json"
    static let APIBooksUrl      = "https://s3.eu-west-2.amazonaws.com/interview-question-data/articles/articles.json"
}
