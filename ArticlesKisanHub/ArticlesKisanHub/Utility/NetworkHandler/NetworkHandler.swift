//
//  NetworkHandler.swift
//  ArticlesKisanHub
//
//  Created by Ketan Patel on 30/09/18.
//  Copyright © 2018 Ketan Patel. All rights reserved.
//

import UIKit

class NetworkHandler: NSObject {

    class func parsingResponse(data: Data?, error:Error?) -> ([String: Any]?, String?){
        
        if(error != nil) {
            print(error?.localizedDescription ?? "")
            return (nil, error?.localizedDescription)
        }
        else {
            
            do {
                ◔false
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    //print("RESPONSE:", json)
                    return (json, nil)
                }
            } catch let error {
                print(error.localizedDescription)
                return (nil, error.localizedDescription)
            }
            return (nil, "api call fail")
        }
    }
    
    class func GETRequset(showLoader: Bool, url:String, callBack:@escaping ([String: Any]?, String?) -> Void) {
        
        ◔showLoader
        
        let request = URLRequest(url: URL(string: url)!)
        
        //print("URL:", url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let (resData,resError) = self.parsingResponse(data: data, error: error)
            callBack(resData,resError)
        }
        task.resume()
    }
}

class BaseClass {
    
    class func parsingResponse(data: Data, error:Error?) -> ([Any]?, String?){
        
        if(error == nil) {
            do {
                ◔false
                
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [Any] {
                    return (json, nil)
                }
                else {
                    return (nil, "api call fail")
                }
            }
            catch let error {
                print(error.localizedDescription)
                return (nil, error.localizedDescription)
            }
        }
        else {
            return (nil, "api call fail")
        }
    }
    
    class func GETRequset(showLoader: Bool, url:String, callBack:@escaping ([Any]?, String?) -> Void) {
        
        ◔showLoader
        
        let request = URLRequest(url: URL(string: url)!)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let (resData,resError) = self.parsingResponse(data: data!, error: error)
            callBack(resData,resError)
        }
        task.resume()
    }
    
}

prefix operator ◔
prefix func ◔(isStart: Bool) {
    DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = isStart
    }
}
